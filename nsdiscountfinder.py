"""                    GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
"""

from typing import Dict, Optional, Tuple, Type
from selenium import webdriver
from selenium.common.exceptions import (
    NoSuchElementException,
    TimeoutException,
    WebDriverException,
)
import smtplib
from email.message import EmailMessage
import time


PriceCouple: Type = Tuple[str, Optional[str]]


DEV_EMAIL = "itashadd+nsdiscountfinder@gmail.com"
"""My email. Use it to contact me if you need help."""

try:
    with open("useremail") as fp:
        USER_EMAIL = fp.read().strip()
except OSError:
    USER_EMAIL = ""

# Configure sender information for notifications
SENDER = {}
"""Data for the sender's email address and SMTP server."""

if USER_EMAIL:
    try:
        with open("smtp") as fp:
            smtp_settings = fp.read()
            smtp_settings = smtp_settings.split(",")
    except OSError:
        smtp_settings = []

    try:
        SENDER["server"] = smtp_settings[0].strip()
        SENDER["login"] = smtp_settings[1].strip()
        SENDER["password"] = smtp_settings[2].strip()
    except IndexError:
        pass

    SENDER.setdefault("server", "localhost")
    SENDER.setdefault("login", "")
    SENDER.setdefault("password", "")

# Configure games to check
GAMES_TO_CHECK: Dict[str, str] = {}
"""Mapping of game names to URL to their Nintendo Store page."""

try:
    with open("gamestocheck.csv") as fp:
        for line in fp:
            entry = line.strip().split(",")
            GAMES_TO_CHECK.update({entry[0].strip(): entry[1].strip()})
except OSError:
    pass

if not GAMES_TO_CHECK:
    raise ValueError("No game to check was detected!")

# Configure driver
driver_options = webdriver.FirefoxOptions()
driver_options.add_argument("--headless")
driver = webdriver.Firefox(options=driver_options)
driver.implicitly_wait(5.0)
driver.set_page_load_timeout(5.0)

waited = False


def check_price(url: str) -> Optional[PriceCouple]:
    """Check the price for the game at the URL and return its regular
    and discounted price, if on discount.
    Exceptionally returns None if the connection times out.
    """

    try:
        driver.get(url)
    except TimeoutException:
        return None

    # Ensure the price display element loads
    global waited
    if not waited:
        time.sleep(5)
        waited = True

    container_xpath = "//div[@id='purchase-options']/div[1]"
    price_div = driver.find_element_by_xpath(container_xpath)
    price_class = price_div.get_attribute("class")

    if price_class is None or "price" not in price_class:
        raise NoSuchElementException

    original_price = driver.find_element_by_xpath(
        f"{container_xpath}/span[contains(@class, 'msrp')]"
    ).text

    price_class: str
    if "discounted" in price_class:
        discounted_price = driver.find_element_by_xpath(
            f"{container_xpath}/span[contains(@class, 'sale-price')]"
        ).text
        return (original_price, discounted_price)

    return (original_price, None)


def send_email(recipient: str, title: str, content: str):
    email = EmailMessage()
    email.set_content(content)
    email["From"] = SENDER["login"]
    email["To"] = recipient
    email["Subject"] = f"Nintendo Store discount finder: {title}"

    with smtplib.SMTP_SSL(SENDER["server"]) as server:
        if SENDER["login"]:
            server.login(SENDER["login"], SENDER["password"])

        server.send_message(email)


if __name__ == "__main__":
    print("Checking prices...")

    notify = []

    try:
        for game_name, game_url in GAMES_TO_CHECK.items():
            # Exponential back-off if the connection times out,
            #  but stop if it gets too long.
            sleep_time = 0
            while (prices := check_price(game_url)) is None:
                sleep_time = sleep_time ** 2 or 2
                if sleep_time >= 256:
                    print(
                        "ERROR: no connection could be established. "
                        "Check your internet connection or try disabling your VPN."
                    )
                    exit(2)

                time.sleep(sleep_time)
                prices = check_price(game_url)

            price_notice = f"{game_name} ({prices[0]})"

            if (discount_price := prices[1]) is None:
                price_notice += " is not discounted."
            else:
                price_notice += (
                    f" is on sale for {discount_price}! Store page: {game_url}"
                )
                notify.append(price_notice)

            print(price_notice)

    except (NoSuchElementException, WebDriverException):
        print(
            f"ERROR: the layout or URL of the page must have changed. "
            f"Download a new version of the script if available, otherwise notify "
            f"the developer at {DEV_EMAIL}."
        )
        exit(1)

    driver.quit()

    if notify and USER_EMAIL:
        n_notif = len(notify)
        email_message = email_title = (
            f"{n_notif} of the games you're watching "
            f"{'is' if n_notif == 1 else 'are'} on sale!"
        )
        email_message += "\n".join(notify)
        send_email(USER_EMAIL, email_title, email_message)

    print("Done.")
