If you wish to improve any part of the program, you are free to open an issue to explain your suggestion or proposal.

Pull requests are also welcome. Just make sure to:

- run `black .` on the code (`pip install black`),
- follow PEP8 to a reasonable degree, and
- be mindful of the style used by the existing code.

Thanks!
