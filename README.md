# Nintendo Store discount notifier
This little script will check the Nintendo Store for discounts on specific games and notify you if appropriate.

If you have any issue with the script, feel free to open an issue on the GitLab repository or 
 [send me an email](mailto:itashadd+nsdiscountfinder@gmail.com?subject=NintendoStoreDiscountNotifier).

Do **star the project** if you like it!

## Usage
Make sure you have Firefox installed and just run `nsdiscountfinder.py` within a Python 3.8+ environment with
 `selenium` and the default `geckodriver` configured. Check the
 [Selenium docs](https://selenium-python.readthedocs.io/installation.html#installation) for info on how to
 install both.

You may want to schedule the execution of the script to periodically check the games.

### Error codes
The script prints the prices and then exits with a code of `0`.

If the script cannot read the store page correctly, it exits with a code of `1`. Retry with an up-to-date version of the
 script; if the newest version doesn't work, contact me and I'll update it.

If the script cannot establish a connection it will retry a few times. If after some time no connection can be made, the
 script exits with a code of `2`. In that case, check your internet connection, firewall and VPN, or retry late.

Make sure to read below. Configuration is quick and easy but required.

## Configuration
### Games to check
Place a file called `gamestocheck.csv` in the script's folder. In it, simply write the **name of the game** (just for your
 convenience; it doesn't have to match the name on the Nintendo Store) and the **URL to its page** on the Nintendo Store.
 **Separate the two with a comma.**

You can insert as many games as you want, **one per line**, and they will all be checked when the script is run.

This step is required: the script will obviously not work if you don't give it at least one game to check.  
Different editions of the same game have different store pages, so they count as separate games. Make sure you add all
 editions you're interested in.

### Email notifications
If you want to receive email notifications, place a file called `useremail` (no extension) in the script's folder. As
 the only content, write the email you want to receive notifications to. You can separate multiple addresses with plain
 commas (no spaces before/after).

This is optional: if no email is provided no email will be sent, and the script will simply print a message in the
 console to notify you of discounts.

The script uses no database so it cannot know whether it has already notified you for the same discount. **Make sure you
 don't run it too often or you'll get too many emails.**

#### SMTP server configuration
By default, the script will attempt to send emails from `localhost`. If you wish to change that (and you probably do),
 put a file called `smtp` inside the script's directory containing a string like so:
 `<server>:<port>,<username>,<password>`, replacing each section (including the angled brackets) with the appropriate
  information. Every section is optional. When in doubt about the port, use SSL.
